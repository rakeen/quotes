The life of a designer is a life of fight. Fight against the ugliness. Just like a doctor fights against disease. For us, the visual disease is what we have around, and what we try to do is cure it somehow with design.” Massimo Vignelli

People who told 'Pen is mightier than the sword' didn't know about guns

But since water still flows, though we cut it with our swords, 
And sorrows return, though we drown them with wine, 
Since the world can in no way answer our craving, 
I will loosen my hair tomorrow and take to a fishingboat. - Li Bai  

Nothing of me is original, I am the combined effort of everybody I've ever known. - Chuck Palahniuk  

There is no objective reality. Instead, we live in perceived realities and are thus responsible for the worldview we adopt.

I was so high I could taste emotion and feel colors. Up was happiness and left was pink.

Improving any skill simply takes time and practice, and you can lengthen the former by increasing the latter. —Jake Rocheleau

You truly fail only if you do nothing. - Simple Pickup Lines(youtube)

I can abide in order to survive. But I can't tolerate insults to Chinese martial arts. - Ip Man 2

Great minds discuss ideas; average minds discuss events; small minds discuss people. - Socrates

Float like a butterfly, sting like a bee. - Muhammad Ali  

Build habits that become systems. Systems beat willpower.

A computer science degree is a great way to get a software developer job at a company that doesn't know how to evaluate programming ability. @nathanmarz  

Treat people who know less than you with respect, deference, and patience.  

Have a competitive spirit, but be a gracious loser. - Jocko Willink  

Be attentive to details but not obsessed with them. Great leaders determine what needs monitoring without losing track of the bigger picture. - Jocko Willink  

If you know you can do better, then do better.  

It's not enough that I shoud succeed - others should fail.  

Think twice to get involved in someone else's problem. Because you wont find anyone in yours.  

When they go low, we go high. - Michelle Obama  

My only advice is, don't ask for advice. - Some nobel laureate  

The only place where success comes before 'try' and 'work' is the dictionary!  

The time we enjoy wasting is never wasted.  

Fools learn from their own mistakes, but clevers learn from others' mistake.

The best things in life are actually really expensive.

Don't hate someone for what they look on the outside, hate them for what a piece of shit they are on the inside.

Be yourself, no one else wants to be you.

If at first you don't succeed, it's probably never going to happen.

Enjoy the good time because something terrible is going to happen.

If you hate yourself remember that you are not alone, a lot of other people hate you too.

True love is when two people lower their standards just the right amount.

People are great as long as you don't get to know them.

Life has to be about more than just solving problems. - Elon Musk

Small mind talks about people, average mind talks about events and great mind talks about ideas.

Don't be a know-it-all; be a learn-it-all. - Satya Nadella  

Let your work speak for you.  

If you have 'passion' for something, it means you haven't really pushed yourself hard enough to see its dark side. - Kaicheng Liang  

When truth suffers the collective consciousness of society suffers. - Hrithik Roshan  

Be ready to go by 5:00; See you at 4:30  

Why join Navy when you can be Pirate?  

Second place is first loser. - Anthony Levandowski(former google engineer | self driving car team)

Success is a lousy teacher, it seduces smart people into thinking they can't lose.

Maybe her contentment undermines my ambition. - Brad's Status  

Although practicality beats purity. Errors should never pass silently. Unless explicitly silenced. - Zen of Python  

Pichware sirf aapne aap ki dhoya ja sakta hai, kisi aur ki nahi. - Saala Khadoos  

Excellence is not fun, it's hard work. - Hichki  

The hottest places in hell are reserved for those who, in times of great moral crisis, maintain their neutrality.  

You're not the only one cursed with knowledge.  - Thanos on Infinity War  

The foolish man seeks happiness in the distance, the wise grow it under his feet. - James Oppenheim  

Productivity boils down to high-velocity decision-making and a bias for action.  

The only way to realy be happy is to fall in love with the process of doing something.

The race is really long and it’s very lonely and the only person you are really racing with is yourself!

Take care of your body. It's the only place you have to live.  

Success is not an act, it is a habit.  

Work until your idol becomes your rival.  

When they say they like you, you don't realy know they like you; but when they say they hate you, you know they really hate you. - Zlatan Ibrahimovic  

It's not who I am underneath, but what I do that defines me - Batman  

You never know how strong you’re until being strong is the only option you have.  

The strong survives, the tough thrives.  

You never want to fail because you didn't work hard enough. - Arnold Schwarzenegger  

You damn the consequences and you power forward. You may get hurt, but nobody wins by doing nothing. You make a choice, you make a move, you go all-in. - Cobra Kai  

Never allow someone to be your priority while allowing yourself to be their option.  

If you surround yourself with clowns, don't be surprised when your life resembles a circus.  

People who are really serious about software, should make their own hardware. - Alan Kay  

Confidence is silent, insecurities are loud.

Just be yourself, it's to late to change.

Do less achieve more.

The master has failed more times than the beginner has even tried.

If you aren't ready to forgive people's mistakes, you will adopt the same stance with yours, and you will never grow as a result.

"Boredom" disappears from your vocabulary once you have fallen in love with learning.

Stop putting yourself down.  Stop doubting how far you’ve come. Stop regretting the choices you’ve made. Satan will make you think otherwise. His job is to make you miserable and depressed. Don’t cave in. Always encourage, support and believe in yourself. That’s the way forward!

Watch out for people who want to teach but not be taught. - twitter@Julian

A teacher who isn't a learner is a chauffeur.

You suddenly become luckier once you are consistent with your efforts. - twitter@orangebook_

One of the richest place in earth is the graveyard

You get to spend your childhood twice when you spend time with your kids
